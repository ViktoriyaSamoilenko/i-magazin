import Vue from 'vue';
import myAlert from '@/utils';
import { MSG_1, MSG_2 } from '@/constants';
import App from './App.vue';

myAlert(MSG_1);
myAlert(MSG_2);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
